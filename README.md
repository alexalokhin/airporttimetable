# AirportTimetable
AirportTimetable is a project that operates with airport flights data.

## Usage
### Load todays flights
Loads today's flights from CSV file.

* **URL**

  `/loadTodaysFlights`

* **Query parameters**

  **Required**

   `filePath=[string]` - path to CSV file. This should be relative to CLASSPATH.

* **Sample request:**

```
'http://localhost:8080/loadTodaysFlights?filePath=flights.csv' \
-H 'Postman-Token: 3c912229-996d-42c8-8e80-f9bbb1fba454' \
-H 'cache-control: no-cache'
```

* **Sample response:**
```
[
  {
    "departureTime": {
      "hour": 11,
      "minute": 5,
      "second": 0,
      "nano": 0
    },
    "destination": "Amsterdam",
    "destinationAirport": "AMS",
    "flightNumber": "CM029",
    "dayOfWeek": "TUESDAY",
    "flightStatus": "PENDING"
  },
  {
    "departureTime": {
      "hour": 12,
      "minute": 20,
      "second": 0,
      "nano": 0
    },
    "destination": "Prague",
    "destinationAirport": "PRG",
    "flightNumber": "CM093",
    "dayOfWeek": "TUESDAY",
    "flightStatus": "PENDING"
  },
  {
    "departureTime": {
      "hour": 10,
      "minute": 15,
      "second": 0,
      "nano": 0
    },
    "destination": "Málaga",
    "destinationAirport": "AGP",
    "flightNumber": "CM063",
    "dayOfWeek": "TUESDAY",
    "flightStatus": "PENDING"
  },
  {
    "departureTime": {
      "hour": 10,
      "minute": 35,
      "second": 0,
      "nano": 0
    },
    "destination": "Berlin",
    "destinationAirport": "TXL",
    "flightNumber": "CM043",
    "dayOfWeek": "TUESDAY",
    "flightStatus": "PENDING"
  },
  {
    "departureTime": {
      "hour": 15,
      "minute": 35,
      "second": 0,
      "nano": 0
    },
    "destination": "Berlin",
    "destinationAirport": "TXL",
    "flightNumber": "CM044",
    "dayOfWeek": "TUESDAY",
    "flightStatus": "PENDING"
  },
  {
    "departureTime": {
      "hour": 11,
      "minute": 45,
      "second": 0,
      "nano": 0
    },
    "destination": "London",
    "destinationAirport": "LGW",
    "flightNumber": "CM027",
    "dayOfWeek": "TUESDAY",
    "flightStatus": "PENDING"
  },
  {
    "departureTime": {
      "hour": 13,
      "minute": 0,
      "second": 0,
      "nano": 0
    },
    "destination": "London",
    "destinationAirport": "LGW",
    "flightNumber": "CM015",
    "dayOfWeek": "TUESDAY",
    "flightStatus": "PENDING"
  }
]
```     