package com.alexanderalokhin.airporttimetable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AirportTimetable {

	public static void main(String[] args) {
		SpringApplication.run(AirportTimetable.class, args);
	}

}
