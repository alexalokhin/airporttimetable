package com.alexanderalokhin.airporttimetable.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alexanderalokhin.airporttimetable.model.Flight;
import com.alexanderalokhin.airporttimetable.service.TimetableService;

@RestController
public class TimetableController {
	@Autowired
	public TimetableService timetableService;

	/**
	 * Loads today's flights from CSV file.
	 * 
	 * @param filePath classpath to CSV file.
	 * @return list of flights in case no exception occurred during processing.
	 *         Response with {@code HttpStatus.BAD_REQUEST)} in case
	 *         {@code FileNotFoundException} is thrown. Response with
	 *         {@code HttpStatus.INTERNAL_SERVER_ERROR} in case {@code IOException}
	 *         is thrown.
	 */
	@RequestMapping("/loadTodaysFlights")
	public ResponseEntity<List<Flight>> loadTodaysFlight(@RequestParam(value = "filePath") String filePath) {
		List<Flight> flights = null;
		try {
			flights = timetableService.getTodaysFlights(filePath);
		} catch (FileNotFoundException e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (IOException e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(flights, HttpStatus.OK);
	}
}
