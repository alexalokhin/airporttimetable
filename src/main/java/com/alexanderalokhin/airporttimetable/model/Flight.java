package com.alexanderalokhin.airporttimetable.model;

import java.time.DayOfWeek;
import java.time.LocalTime;

/**
 * Represents record in CSV file.
 * 
 * @author oalokhin
 *
 */
public class Flight {
	private LocalTime departureTime;
	private String destination;
	private String destinationAirport;
	private String flightNumber;
	private DayOfWeek dayOfWeek;
	private FlightStatus flightStatus;

	public LocalTime getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(LocalTime departureTime) {
		this.departureTime = departureTime;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDestinationAirport() {
		return destinationAirport;
	}

	public void setDestinationAirport(String destinationAirport) {
		this.destinationAirport = destinationAirport;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public DayOfWeek getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(DayOfWeek dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public FlightStatus getFlightStatus() {
		return flightStatus;
	}

	public void setFlightStatus(FlightStatus flightStatus) {
		this.flightStatus = flightStatus;
	}

	@Override
	public String toString() {
		return "Flight [departureTime=" + departureTime + ", destination=" + destination + ", destinationAirport="
				+ destinationAirport + ", flightNumber=" + flightNumber + ", dayOfWeek=" + dayOfWeek + ", flightStatus="
				+ flightStatus + "]";
	}

}