package com.alexanderalokhin.airporttimetable.model;

/**
 * Defines possible flight statuses.
 * 
 * @author oalokhin
 *
 */
public enum FlightStatus {
	PENDING, DEPARTED
}
