package com.alexanderalokhin.airporttimetable.service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.alexanderalokhin.airporttimetable.model.Flight;
import com.alexanderalokhin.airporttimetable.model.FlightStatus;

/**
 * Contains methods for airport timetable data processing.
 * 
 * @author oalokhin
 *
 */
@Service
public class TimetableService {
	private static final String SEPARATOR = ",";
	private static String[] headers;

	/**
	 * Loads and processes data from input CSV file which contains predefined
	 * information about flights.
	 * 
	 * @param classpath to CSV file
	 * @return list of today's flights
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public List<Flight> getTodaysFlights(String filePath) throws FileNotFoundException, IOException {
		List<Flight> flights = null;
		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(getClass().getClassLoader().getResourceAsStream(filePath)))) {
			if (headers == null) {
				Optional<String> firstRecord = br.lines().findFirst();
				if (firstRecord.isPresent()) {
					headers = firstRecord.get().split(SEPARATOR);
				}
			}
			flights = br.lines().skip(1).map(mapToFlight).flatMap(List::stream).collect(Collectors.toList());
		}
		if (flights != null) {
			DayOfWeek currentDayOfWeek = LocalDate.now().getDayOfWeek();
			flights = flights.stream().filter(flight -> flight.getDayOfWeek().equals(currentDayOfWeek))
					.collect(Collectors.toList());
			LocalTime now = LocalTime.now();
			flights.stream().filter(flight -> flight.getDepartureTime().isBefore(now))
					.forEach(flight -> flight.setFlightStatus(FlightStatus.DEPARTED));
			flights.stream().filter(flight -> flight.getDepartureTime().isAfter(now))
					.forEach(flight -> flight.setFlightStatus(FlightStatus.PENDING));
		}
		return flights;
	}

	/**
	 * Mapper function that converts CSV record to {@code Link<Flight>} object.
	 */
	private Function<String, List<Flight>> mapToFlight = (line) -> {
		List<Flight> flights = new ArrayList<>();
		String[] flightCsvRecordDetails = line.split(SEPARATOR);
		for (int i = 4; i < flightCsvRecordDetails.length; i++) {
			// we use 4 as a starting index of dayofweek column
			if (!flightCsvRecordDetails[i].isEmpty()) {
				Flight flight = new Flight();
				flight.setDepartureTime(LocalTime.parse(flightCsvRecordDetails[0]));
				flight.setDestination(flightCsvRecordDetails[1]);
				flight.setDestinationAirport(flightCsvRecordDetails[2]);
				flight.setFlightNumber(flightCsvRecordDetails[3]);
				flight.setDayOfWeek(DayOfWeek.valueOf(headers[i].toUpperCase()));
				flights.add(flight);
			}
		}
		return flights;
	};

}